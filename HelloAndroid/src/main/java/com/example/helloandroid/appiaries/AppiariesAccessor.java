package com.example.helloandroid.appiaries;

import android.net.Uri;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by mid on 14/04/22.
 */
public final class AppiariesAccessor {

    public static final String SCHEME = "https";
    public static final String BASE_URL = "api-datastore.appiaries.com";
    public static final String APP_ID = "demo_shop_mid";
    public static final String DATASTORE_ID = "_sandbox";
    private final String storeToken;

    private static final AppiariesAccessor accessor = new AppiariesAccessor();

    private static final String TAG = AppiariesAccessor.class.getName();

    private AppiariesAccessor() {
        storeToken = "{appiaries stored token}";
    }

    public static AppiariesAccessor getInstance() {
        return accessor;
    }

    public void registId(String regid) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(SCHEME);
        builder.encodedAuthority(BASE_URL);
        builder.path("/v1/push/gcm/" + DATASTORE_ID + "/" + APP_ID + "/_target");
        builder.appendQueryParameter("access_token", storeToken);

        HttpPut request = new HttpPut(builder.build().toString());
        JSONObject body = new JSONObject();
        try {
            body.put("regid", regid);
            request.setEntity(new StringEntity(body.toString()));
            request.setHeader("Content-Type", "application/json");
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage(), e);
        }

        DefaultHttpClient httpClient = new DefaultHttpClient();

        try {
            String result = null;
            try {
                result = httpClient.execute(request, new ResponseHandler<String>() {
                    @Override
                    public String handleResponse(HttpResponse response)
                            throws ClientProtocolException, IOException {

                        // response.getStatusLine().getStatusCode()でレスポンスコードを判定する。
                        // 正常に通信できた場合、HttpStatus.SC_OK（HTTP 200）となる。
                        switch (response.getStatusLine().getStatusCode()) {
                            case HttpStatus.SC_NO_CONTENT:
                                return "";

                            case HttpStatus.SC_NOT_FOUND:
                                throw new RuntimeException("データないよ！"); //FIXME

                            default:
                                throw new RuntimeException("なんか通信エラーでた"); //FIXME
                        }

                    }
                });
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
            }
        } finally {
            // ここではfinallyでshutdown()しているが、HttpClientを使い回す場合は、
            // 適切なところで行うこと。当然だがshutdown()したインスタンスは通信できなくなる。
            httpClient.getConnectionManager().shutdown();
        }
    }

}
